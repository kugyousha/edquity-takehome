import React from 'react';

import { LeftPanel } from './components/LeftPanel';
import { TopNavigation } from './components/TopNavigation';
import { TestChart } from './components/TestChart';
import './App.scss';

const App: React.FC = () => {
  return (
    <div className="App">
      <LeftPanel />
      <div className="main-section">
        <TopNavigation />
        <div className="content-area">
          <section className="realtime-messages">
            <div className="section-heading">
              <div className="section-title">Realtime Message Data</div>
              <div className="options-dropdown">
                Options
                <i className="fa fa-chevron-down"></i>
              </div>
            </div>
            <div className="card-container">
              <section className="message-card purple">
                <div className="top-half">
                  <div className="icon-wrapper">
                    <div className="inner-wrapper">
                      <i className="fa fa-play-circle-o"></i>
                    </div>
                  </div>
                  Started sending at
                </div>
                <div className="bottom-half">
                  <div className="primary-info">March 27th 2019</div>
                  <div className="secondary-info">12:26:05 am UTC - 07:00</div>
                </div>
              </section>
              <section className="message-card orange">
                <div className="top-half">
                  <div className="icon-wrapper">
                    <div className="inner-wrapper">
                      <i className="fa fa-clock-o"></i>
                    </div>
                  </div>
                  Completed
                </div>
                <div className="bottom-half">
                  <div className="primary-info">In 31.28</div>
                  <div className="secondary-info">seconds</div>
                </div>
              </section>
              <section className="message-card pink">
                <div className="top-half">
                  <div className="icon-wrapper">
                    <div className="inner-wrapper">
                      <i className="fa fa-commenting-o"></i>
                    </div>
                  </div>
                  Total Messages
                </div>
                <div className="bottom-half">
                  <div className="primary-info">3522725</div>
                </div>
              </section>
              <section className="message-card turquoise">
                <div className="top-half">
                <div className="icon-wrapper">
                  <div className="inner-wrapper">
                    <i className="fa fa-check-circle-o"></i>
                  </div>
                </div>
                  Status
                </div>
                <div className="bottom-half">
                  <div className="primary-info">Delivered</div>
                </div>
              </section>
            </div>
          </section>
          <section className="upload-chart">
            <div className="section-heading">
              <div className="section-title secondary">Upload your data</div>
              <div className="chart-navigation">
                <span className="time-option">Last 120 seconds</span>
                <span className="time-option">Last 30 minutes</span>
                <span className="time-option active">Last 24 hours</span>
                <span className="time-option">Last 5 days</span>
              </div>
            </div>
            <div className="chart-container">
              <TestChart />
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default App;
