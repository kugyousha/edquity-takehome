import React from 'react';
import { Chart } from 'react-charts';

export const TestChart: React.FC = () => {
  const data = React.useMemo(
    () => [
      {
        label: "Series 1",
        data: [
          ["12 am", 250],
          ["2 am", 350], 
          ["4 am", 375],
          ["6 am", 250], 
          ["8 am", 210],
          ["10 am", 250],
          ["12 pm", 230],
          ["14 pm", 200],
          ["16 pm", 140], 
          ["18 pm", 120],
          ["20 pm", 150],
          ["22 pm", 220]
        ]
      }
    ],
    []
  );

  const axes = React.useMemo(
    () => [
      { primary: true, type: "ordinal", position: "bottom" },
      { type: "linear", position: "left" }
    ],
    []
  );

  return (
    <Chart data={data} axes={axes} tooltip/>
  );
}
