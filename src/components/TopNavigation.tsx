import React from 'react';

import './TopNavigation.scss';

export const TopNavigation: React.FC = () => {
  return (
    <section className="top-navigation">
      <div className="search-container">
        <i className="fa fa-search"></i>
        <input placeholder="Type in to search"/>
      </div>
      
      <div className="secondary-heading">Cloud Four - Productions</div>
      <div className="user-controls">
        <div className="faux-user-picture"></div>
        <i className="fa fa-chevron-down"></i>
      </div>
    </section>
  );
}
