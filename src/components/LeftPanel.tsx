import React from 'react';

import './LeftPanel.scss';

export const LeftPanel: React.FC = () => {
  return (
    <nav className="left-panel">
      <header className="company-name">Edquity</header>
      <section className="main-navigation">
        <ul className="side-navigation">
          <li className="navigation-item">
            <i className="fa fa-podcast"></i>
            Dashboard
          </li>
          <li className="navigation-item">
            <i className="fa fa-sliders"></i>
            Settings
          </li>
          <li className="navigation-item">
            <i className="fa fa-user-circle-o"></i>
            Users
          </li>
          <li className="navigation-item active">
            <i className="fa fa-envelope-o"></i>
            Delivery
          </li>
          <li className="navigation-item">
            <i className="fa fa-wifi"></i>
            OneSignal API
          </li>
        </ul>
      </section>
      <section className="secondary-navigation">
        <div className="secondary-heading">
          Concepts
        </div>
        <ul className="side-navigation">
          <li className="navigation-item">
            <i className="fa fa-info-circle"></i>
            Prompting
          </li>
          <li className="navigation-item">
            <i className="fa fa-bell-o"></i>
            Notification Appearance
          </li>
          <li className="navigation-item">
            <i className="fa fa-comment-o"></i>
            Message Personalization
          </li>
          <li className="navigation-item">
            <i className="fa fa-id-badge"></i>
            Player ID
          </li>
          <li className="navigation-item">
            <i className="fa fa-square-o"></i>
            Notification Behavior
          </li>
        </ul>
      </section>
    </nav>
  );
}
